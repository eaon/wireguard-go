module golang.zx2c4.com/wireguard

go 1.12

require (
	golang.org/x/crypto v0.0.0-20190617133340-57b3e21c3d56
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	golang.org/x/sys v0.0.0-20190618155005-516e3c20635f
)
